
# Unibuild files for Pantheon packages.

***Use unibuild for build packages!***


## Packages (pantheon-tearch):

### General
- granite
- cerbere #removed
- contractor
- gala
- pantheon-dpms-helper
- pantheon-polkit-agent

### Switchboard
- switchboard
- switchboard-plug-about
- switchboard-plug-power
- switchboard-plug-applications
- switchboard-plug-parental-controls
- switchboard-plug-online-accounts (removed)
- switchboard-plug-datetime
- switchboard-plug-notifications
- switchboard-plug-mouse-touchpad
- switchboard-plug-desktop
- switchboard-plug-display
- switchboard-plug-printers
- switchboard-plug-bluetooth
- switchboard-plug-sharing
- switchboard-plug-sound
- switchboard-plug-elementary-tweaks
- switchboard-plug-user-accounts
- switchboard-plug-a11y
- switchboard-plug-network
- switchboard-plug-keyboard

### Wingpanel and Appmenu
- wingpanel
- wingpanel-indicator-session
- wingpanel-indicator-bluetooth
- wingpanel-indicator-nightlight
- wingpanel-indicator-sound
- wingpanel-indicator-power
- wingpanel-indicator-network
- wingpanel-indicator-notifications
- wingpanel-indicator-datetime
- wingpanel-indicator-namarupa
- wingpanel-indicator-keyboard
- wingpanel-indicator-a11y
- pantheon-applications-menu

### Miscellaneous
- pantheon-default-settings (removed)
- elementary-icon-theme
- pantheon-session

## Packages (pantheon-extra-tearch):

### Core Applications
- capnet-assist 
- pantheon-calculator 
- pantheon-calendar
- pantheon-camera 
- pantheon-code 
- pantheon-files
- pantheon-music 
- pantheon-photos
- pantheon-screencast 
- pantheon-screenshot 
- pantheon-shortcut-overlay 
- pantheon-terminal 
- pantheon-videos

### Extra Applications
- [appeditor](https://appcenter.elementary.io/com.github.donadigo.appeditor/) 
- [desktopfolder](https://appcenter.elementary.io/com.github.spheras.desktopfolder/) 
- [planner](https://appcenter.elementary.io/com.github.alainm23.planner/) 

### Miscellaneous
- gtk-theme-elementary
- elementary-wallpapers
